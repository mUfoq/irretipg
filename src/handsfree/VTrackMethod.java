/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package handsfree;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ufoq
 */
public class VTrackMethod extends CHMethod {

	@Override
	public void onRun() {
		final Robot robot;

		final VTrackInfo trackX = new VTrackInfo();
		final VTrackInfo trackY = new VTrackInfo();

		try {
			robot = new Robot();
		} catch (AWTException ex) {
			Logger.getLogger(HandsFree.class.getName()).log(Level.SEVERE, null, ex);
			return;
		}

		try {
			Rectangle processFrame;
			while ((processFrame = cameraHandler.getProcessFrame()) == null) {
				if (!isRunning()) {
					return;
				}
			}

			final ScalingConfigXY scalingConfig = new ScalingConfigXY(processFrame.width, processFrame.height);
			int px = processFrame.width / 2 + processFrame.x;
			int py = processFrame.height / 2 + processFrame.y;
			scalingConfig.setRangeX(px, px);
			scalingConfig.setRangeY(py, py);

			cameraHandler.setResultListener(CameraHandler.SearchType.FACE, new CameraHandler.OnResultListener() {
				@Override
				public void onResult(Rectangle r, Point p) {
					if (p != null) {
						scalingConfig.recordX(p.x);
					}
				}
			});

			cameraHandler.setResultListener(CameraHandler.SearchType.EYEPAIR, new CameraHandler.OnResultListener() {
				@Override
				public void onResult(Rectangle r, Point p) {
					if (p != null) {
						scalingConfig.recordY(p.y);
					}
				}
			});

			Log.d("przez 5s, bede sprawdzal max wychylenie");

			long startTime = System.currentTimeMillis();
			while (true) {
				
				if (!isRunning()) {
					return;
				}

				if (System.currentTimeMillis() - startTime > 5000) {
					if (scalingConfig.getRatioWidth() < 0.005 || scalingConfig.getRatioHeight() < 0.005) {
						Log.d("za male wychylenia, jeszcze 5s ...");
						startTime = System.currentTimeMillis();
					} else {
						break;
					}
				}

				Rectangle pf = cameraHandler.getProcessFrame();
				if (pf != null) {
					processFrame = pf;
				}
			}

			Point ppf = CameraHandler.midPoint(processFrame);
			int x = ppf.x;
			int y = ppf.y;
			trackX.faceJustFound(x);
			trackY.faceJustFound(y);

			cameraHandler.setResultListener(CameraHandler.SearchType.FACE, new CameraHandler.OnResultListener() {
				@Override
				public void onResult(Rectangle r, Point p) {
					if (p != null) {
						trackX.updatePosition(p.x);
						double scaleX = scalingConfig.getRatioWidth();
						double mx = trackX.getMove() / scaleX;
						int movex = (int) (-mx);
						if (movex != 0) {
							Point mousePos = MouseInfo.getPointerInfo().getLocation();
							robot.mouseMove(mousePos.x + movex, mousePos.y);
						}
					}
				}
			});

			cameraHandler.setResultListener(CameraHandler.SearchType.EYEPAIR, new CameraHandler.OnResultListener() {
				@Override
				public void onResult(Rectangle r, Point p) {
					if (p != null) {
						trackY.updatePosition(p.y);
						double scaleY = scalingConfig.getRatioHeight();
						double my = trackY.getMove() / scaleY;
						int movey = (int) (my);
						if (movey != 0) {
							Point mousePos = MouseInfo.getPointerInfo().getLocation();
							robot.mouseMove(mousePos.x, mousePos.y + movey);
						}
					}
				}
			});

			Log.d("go");
			while (isRunning()) {
				cameraHandler.getProcessFrame();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}