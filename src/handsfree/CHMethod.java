/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package handsfree;

import java.awt.Rectangle;

/**
 *
 * @author ufoq
 */
public abstract class CHMethod extends Thread {

	public interface OnMethodFinishListener {

		public void onFinish();
	}
	public static final CameraHandler cameraHandler = new CameraHandler();
	private OnMethodFinishListener onMethodFinishListener = null;
	private boolean running = false;

	@Override
	public final void run() {
		setRunning(true);
		onRun();
		onFinish();
	}

	public synchronized void finish(OnMethodFinishListener onMethodFinishListener) {
		this.onMethodFinishListener = onMethodFinishListener;
		setRunning(false);
	}

	protected abstract void onRun();

	protected synchronized void onFinish() {
		cameraHandler.resetResultListeners();
		if (onMethodFinishListener != null) {
			onMethodFinishListener.onFinish();
		}
	}

	protected synchronized boolean isRunning() {
		return running;
	}

	protected synchronized void setRunning(boolean v) {
		running = v;
	}

	protected Rectangle getFrame() throws Exception {
		return cameraHandler.getProcessFrame();
	}
}
