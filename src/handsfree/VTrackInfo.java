package handsfree;

/**
 *
 * @author ufoq
 */
public class VTrackInfo {

	Integer origin;
	int position;
	long positionChangedTimestamp;
	int move = 0;

	void faceJustFound(int p) {
		setOrigin(p);
	}

	void setOrigin(int pos) {
		origin = pos;
		setPosition(pos);
	}

	void setPosition(int pos) {
		position = pos;
		positionChangedTimestamp = System.currentTimeMillis();
	}

	void updatePosition(Integer pos) {
		move = 0;

		if (pos == null) {
			origin = null;
			return;
		}

		if (origin == null || isPositionChangeTimeout()) {
			setOrigin(pos);
			return;
		}

		int localDiff = pos - position;
		if (!isPositionChangeEnough(localDiff)) {
			return;
		}

		int positionOriginDiff = position - origin;

		// jesli pomiedzy position a origin, to ignore
		if (btwnp(pos, origin, position)) {
			return;
		}
		// jesli rosnie w ta sama strone co positionOriginDiff
		/// to dodawaj localDiff

		if (Math.signum(localDiff) == Math.signum(positionOriginDiff)) {
			move = localDiff;
			setPosition(pos);
		} else {
			// jesli jest po drugiej stronie odniesienia
			/// to policz offset od origin i jesli jest wystarczajacy to go dodaj
			/// jak nie to ustaw last position w origin
			int p = pos - origin;
			if (isPositionChangeEnough(p)) {
				setPosition(pos);
			} else {
				setPosition(origin);
			}

		}
	}

	int getMove() {
		return move;
	}

	boolean isPositionChangeTimeout() {
		return (System.currentTimeMillis()
				- positionChangedTimestamp) > 2000;
	}

	boolean isPositionChangeEnough(int d) {
		return d != 0;
	}

	private static boolean btwnp(int x, int a, int b) {
		int min = a < b ? a : b;
		int max = a > b ? a : b;
		return x >= min && x <= max;
	}
}
