/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package handsfree.ui;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.cpp.opencv_core;

/**
 *
 * @author ufoq
 */
public class CameraPreview {
	
	private static CameraPreview instance = null;
	
	public static CameraPreview get() {
		if (instance == null) {
			instance = new CameraPreview();
		}
		return instance;
	}
	private CanvasFrame canvasFrame = new CanvasFrame("Camera Preview");
	
	private CameraPreview() {
	}
	
	public void putFrame(opencv_core.IplImage grabbedImage) {
		if (canvasFrame.isVisible() && grabbedImage != null) {
			canvasFrame.showImage(grabbedImage);
		}
	}
}
