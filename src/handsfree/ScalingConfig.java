package handsfree;

/**
 *
 * @author ufoq
 */
public class ScalingConfig {

	private int maxValue;
	private int usableValue;
	private int minV, maxV;

	public ScalingConfig(int maxValue) {
		this.maxValue = maxValue;
	}

	public void setMaxValue(int v) {
		maxValue = v;
	}

	public void setRange(int v1, int v2) {
		if (v2 > v1) {
			minV = v1;
			maxV = v2;
		} else {
			minV = v2;
			maxV = v1;
		}
		updateUsableSize();
	}

	public void record(int v) {
		if (v < minV) {
			minV = v;
		} else if (v > maxV) {
			maxV = v;
		}
		updateUsableSize();
	}

	public double getRatio() {
		return usableValue / (double) maxValue;
	}

	public double getMiddleValue() {
		return (minV + maxV) / 2.0;
	}
	
	public double getUsableValue() {
		return usableValue;
	}

	private void updateUsableSize() {
		//Log.d("min: "+minV+" max: "+maxV);
		usableValue = maxV - minV;
	}
}
