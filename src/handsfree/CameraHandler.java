package handsfree;

import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.OpenCVFrameGrabber;
import static com.googlecode.javacv.cpp.opencv_core.*;
import com.googlecode.javacv.cpp.opencv_core.CvMemStorage;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
import com.googlecode.javacv.cpp.opencv_objdetect;
import com.googlecode.javacv.cpp.opencv_objdetect.CvHaarClassifierCascade;
import static com.googlecode.javacv.cpp.opencv_objdetect.cvHaarDetectObjects;
import handsfree.ui.CameraPreview;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.EnumMap;

/**
 *
 * @author mASTEr
 * @author ufoq
 */
public class CameraHandler {

	public static interface OnResultListener {

		public void onResult(Rectangle r, Point p);
	}

	public static interface OnFrameListener {

		public void onFrame(IplImage image);
	}

	public static enum SearchType {

		FACE, EYEPAIR
	}

	public static Point midPoint(Rectangle r) {
		return new Point(r.x + r.width / 2, r.y + r.height / 2);
	}

	private static CvHaarClassifierCascade loadHaar(String filename) {
		CvHaarClassifierCascade ccc = new opencv_objdetect.CvHaarClassifierCascade(
				com.googlecode.javacv.cpp.opencv_core.cvLoad("res/" + filename));

		if (ccc.isNull()) {
			System.out.println(filename + " classifier load failed.");
		}

		return ccc;
	}
	private FrameGrabber camera;
	private CvHaarClassifierCascade classifierFace;
	private CvHaarClassifierCascade classifierEyes;
	private EnumMap<SearchType, ArrayList<OnResultListener>> listeners =
			new EnumMap<SearchType, ArrayList<OnResultListener>>(SearchType.class);
	private OnFrameListener onFrameListener;
	public int camWidth, camHeight;

	public CameraHandler() {
		Loader.load(opencv_objdetect.class);
	}

	public int init(int deviceNumber) {
		classifierEyes = loadHaar("haarcascade_mcs_eyepair_big.xml");
		classifierFace = loadHaar("haarcascade_frontalface_default.xml");

		int result = 0;
		try {
			this.camera = new OpenCVFrameGrabber(deviceNumber);
			this.camera.start();
			camHeight = this.camera.getImageHeight();
			camWidth = this.camera.getImageWidth();
		} catch (Exception e) {
			result = -1;
		}
		return result;
	}

	public void setResultListener(SearchType st, OnResultListener rl) {
		if (rl == null) {
			listeners.remove(st);
			return;
		}
		if (listeners.get(st) == null) {
			listeners.put(st, new ArrayList<OnResultListener>());
		} else {
			listeners.get(st).clear();
		}
		listeners.get(st).add(rl);
	}

	public void addResultListener(SearchType st, OnResultListener rl) {
		if (rl == null) {
			listeners.remove(st);
			return;
		}
		if (listeners.get(st) == null) {
			listeners.put(st, new ArrayList<OnResultListener>());
		}
		listeners.get(st).add(rl);
	}

	public void setOnFrameListener(OnFrameListener ofl) {
		onFrameListener = ofl;
	}

	public void resetResultListeners() {
		listeners.clear();
		onFrameListener = null;
	}
	Rectangle lastRectangle;

	public Rectangle getProcessFrame() throws Exception {
		IplImage frame;

		int Width;
		int Height;

		frame = camera.grab();

		Width = frame.width();
		Height = frame.height();

		IplImage grayImage = IplImage.create(Width, Height, IPL_DEPTH_8U, 1);

		CvMemStorage memory = CvMemStorage.create();

		frame = camera.grab();

		if (frame.isNull() == false) {

			CvSeq faces;

			// clear the memory storage 

			cvClearMemStorage(memory);

			cvCvtColor(frame, grayImage, com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY);

			CvRect cr = null;

			if (lastRectangle != null) {

				final Rectangle r = lastRectangle;

				final int fw = grayImage.width();
				final int fh = grayImage.height();

				final int bw = (int) (r.width * 0.1f);
				final int bh = (int) (r.height * 0.1f);
				int x = r.x - bw;
				if (x < 0) {
					x = 0;
				}

				int y = r.y - bh;
				if (y < 0) {
					y = 0;
				}

				int w = r.width + 2 * bw;
				if (x + w > fw) {
					w = fw - x;
				}

				int h = r.height + 2 * bh;
				if (y + h > fh) {
					h = fh - y;
				}


				cvSetImageROI(grayImage, cvRect(x, y, w, h));

				faces = cvHaarDetectObjects(grayImage,
						classifierFace,
						memory,
						1.1,
						3,
						com.googlecode.javacv.cpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING);

				for (int i = 0; i < faces.total(); i++) {
					CvRect tmp = new CvRect(cvGetSeqElem(faces, i));

					if (cr == null || tmp.width() * tmp.height() > cr.width() * cr.height()) {
						tmp.x(tmp.x() + x);
						tmp.y(tmp.y() + y);
						cr = tmp;
					}
				}

				if (cr != null && cr.width() * cr.height() < (r.width * r.height) / 2) {
					cr = null;
				}
			}

			if (cr == null) {
				cvSetImageROI(grayImage, cvRect(0, 0, grayImage.width(), grayImage.height()));
				faces = cvHaarDetectObjects(grayImage,
						classifierFace,
						memory,
						1.3,
						3,
						com.googlecode.javacv.cpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING);

				for (int i = 0; i < faces.total(); i++) {
					CvRect tmp = new CvRect(cvGetSeqElem(faces, i));

					if (cr == null || tmp.width() * tmp.height() > cr.width() * cr.height()) {
						cr = tmp;
					}
				}
			}

			CvRect mr = null;
			CvRect smiler = null;
			if (cr != null) {
				cvRectangle(frame, cvPoint(cr.x(), cr.y()),
						cvPoint(cr.x() + cr.width(), cr.y() + cr.height()),
						CvScalar.YELLOW, 1, CV_AA, 0);

				cvSetImageROI(grayImage, cr);

				faces = cvHaarDetectObjects(grayImage,
						classifierEyes,
						memory,
						1.1,
						3,
						com.googlecode.javacv.cpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING);


				for (int i = 0; i < faces.total(); i++) {
					CvRect tmp = new CvRect(cvGetSeqElem(faces, i));

					if (mr == null || tmp.width() * tmp.height() > mr.width() * mr.height()) {
						tmp.x(tmp.x() + cr.x());
						tmp.y(tmp.y() + cr.y());
						mr = tmp;
					}
				}

				if (mr != null) {
					cvRectangle(frame, cvPoint(mr.x(), mr.y()),
							cvPoint(mr.x() + mr.width(), mr.y() + mr.height()),
							CvScalar.BLUE, 1, CV_AA, 0);
				}

			}

			OnFrameListener ofl = onFrameListener;
			if (ofl != null) {
				ofl.onFrame(frame);
			}

			CameraPreview.get().putFrame(frame);

			Rectangle rectangle = cr == null ? null : new Rectangle(cr.x(), cr.y(), cr.height(), cr.width());
			grayImage.release();
			lastRectangle = rectangle;

			notifyResultListeners(SearchType.FACE, rectangle);

			notifyResultListeners(SearchType.EYEPAIR, mr);

			return rectangle;
		}

		return null;
	}

	private void notifyResultListeners(SearchType st, CvRect mr) {
		if (mr != null) {
			notifyResultListeners(st, new Rectangle(mr.x(), mr.y(), mr.height(), mr.width()));
		} else {
			notifyResultListeners(st, (Rectangle) null);
		}
	}

	private void notifyResultListeners(SearchType st, Rectangle rectangle) {
		ArrayList<OnResultListener> arr = listeners.get(st);
		if (arr != null) {
			for (OnResultListener facerl : arr) {
				if (rectangle == null) {
					facerl.onResult(null, null);
				} else {
					facerl.onResult(rectangle, midPoint(rectangle));
				}
			}
		}
	}
}
