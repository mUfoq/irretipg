package handsfree;

import handsfree.ui.MainFrame;

/**
 *
 * @author mASTEr
 * @author ufoq
 */
public class HandsFree {

	private static CHMethod chmethod = null;

	public static void startCHMethod(CHMethod chmethod) {
		CHMethod old = HandsFree.chmethod;
		HandsFree.chmethod = chmethod;
		if (old == null) {
			chmethod.start();
		} else {
			old.finish(new CHMethod.OnMethodFinishListener() {
				@Override
				public void onFinish() {
					HandsFree.chmethod.start();
				}
			});
		}

	}

	public static void main(String[] args) {
		CHMethod.cameraHandler.init(0);

		new MainFrame().setVisible(true);
	}
}
