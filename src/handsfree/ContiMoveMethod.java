/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package handsfree;

import com.googlecode.javacv.cpp.opencv_core;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ufoq
 */
public class ContiMoveMethod extends CHMethod {

	private static class OffsetCounter {

		private int middle;
		private long timestamp = System.currentTimeMillis();
		private double fornext = 0;

		public OffsetCounter(int middle) {
			this.middle = middle;
		}

		public int getChange(int value, double usable, int maxValue) {
			int diff = value - middle;
			double c = ((double) diff) / usable;
			double sign = Math.signum(c);
			c = Math.abs(c);
			if (c > 1) {
				c = 1;
			} else if (c < 0.005) {
				return 0;
			} else if (c < 0.5) {
				c = c * c;
			}

			long nt = System.currentTimeMillis();
			long timediff = nt - timestamp;
			timestamp = nt;

			if (timediff > 1000) {
				return 0;
			}

			c = Math.pow(c, 3) * maxValue * (timediff / 10000.0);
			c = c * sign;
			c = c + fornext;
			int trunc = (int) c;
			fornext = (c - trunc);
			return (int) c;
		}
	}

	private static abstract class ConfigAdjuster extends ScalingConfig implements CameraHandler.OnResultListener {

		private long ts = System.currentTimeMillis();

		public ConfigAdjuster(int maxValue, int initialValue) {
			super(maxValue);
			this.setRange(initialValue, initialValue);
		}

		@Override
		public void onResult(Rectangle r, Point p) {
			if (p != null && System.currentTimeMillis() - ts > 1000) {
				onPoint(p);
			}
		}

		protected abstract void onPoint(Point p);
	}
	private IplImage midframe;
	private Point _p;

	@Override
	public void onRun() {
		final Robot robot;
		
		try {
			robot = new Robot();
		} catch (AWTException ex) {
			Logger.getLogger(HandsFree.class.getName()).log(Level.SEVERE, null, ex);
			return;
		}

		try {
			Rectangle processFrame;
			while ((processFrame = getFrame()) == null) {
				if (!isRunning()) {
					return;
				}
			}

			final ConfigAdjuster configX = new ConfigAdjuster(cameraHandler.camWidth,
					processFrame.width / 2 + processFrame.x) {
				@Override
				protected void onPoint(Point p) {
					_p = p;
					record(p.x);
				}
			};

			final ConfigAdjuster configY = new ConfigAdjuster(cameraHandler.camHeight,
					processFrame.height / 2 + processFrame.y) {
				@Override
				protected void onPoint(Point p) {
					record(p.y);
				}
			};

			// szukam srodeczka

			CameraHandler.SearchType stForY = CameraHandler.SearchType.FACE;

			cameraHandler.setResultListener(CameraHandler.SearchType.FACE, configX);
			cameraHandler.addResultListener(stForY, configY);
			cameraHandler.setOnFrameListener(new CameraHandler.OnFrameListener() {
				@Override
				public void onFrame(IplImage frame) {
					midframe = frame;

					int midX = (int) configX.getMiddleValue();
					int midY = (int) configY.getMiddleValue();

					final opencv_core.CvRect midRect;
					{
						int pad = 5;
						int x = midX - pad;
						if (x < 0) {
							x = 0;
						}
						int y = midY - pad;
						if (y < 0) {
							y = 0;
						}
						int w = pad * 2;
						if (x + w > frame.width()) {
							w = frame.width() - x;
						}
						int h = pad * 2;
						if (y + h > frame.height()) {
							h = frame.height() - y;
						}
						midRect = new opencv_core.CvRect(x, y, w, h);
					}
					opencv_core.CvRect mr = midRect;
					opencv_core.cvRectangle(frame, opencv_core.cvPoint(mr.x(), mr.y()),
							opencv_core.cvPoint(mr.x() + mr.width(), mr.y() + mr.height()),
							opencv_core.CvScalar.GREEN, 1, opencv_core.CV_AA, 0);
				}
			});

			Log.d("przez 3s, szukam srodka");

			long startTime = System.currentTimeMillis();
			while (System.currentTimeMillis() - startTime < 3000 || midframe == null) {
				if (!isRunning()) {
					return;
				}
				Rectangle pf = getFrame();
				if (pf != null) {
					processFrame = pf;
				}
			}

			//int midX = (int) configX.getMiddleValue();
			//int midY = (int) configY.getMiddleValue();
			int midX = _p.x;
			int midY = _p.y;
			final OffsetCounter ocX = new OffsetCounter(midX);
			final OffsetCounter ocY = new OffsetCounter(midY);

			final opencv_core.CvRect midRect;
			{
				int pad = 5;
				int x = midX - pad;
				if (x < 0) {
					x = 0;
				}
				int y = midY - pad;
				if (y < 0) {
					y = 0;
				}
				int w = pad * 2;
				if (x + w > midframe.width()) {
					w = midframe.width() - x;
				}
				int h = pad * 2;
				if (y + h > midframe.height()) {
					h = midframe.height() - y;
				}
				midRect = new opencv_core.CvRect(x, y, w, h);
			}

			cameraHandler.setOnFrameListener(new CameraHandler.OnFrameListener() {
				@Override
				public void onFrame(IplImage frame) {
					opencv_core.CvRect mr = midRect;
					opencv_core.cvRectangle(frame, opencv_core.cvPoint(mr.x(), mr.y()),
							opencv_core.cvPoint(mr.x() + mr.width(), mr.y() + mr.height()),
							opencv_core.CvScalar.GREEN, 1, opencv_core.CV_AA, 0);
				}
			});

			Log.d("przez 5s, bede sprawdzal max wychylenie");

			startTime = System.currentTimeMillis();
			while (true) {
				if (!isRunning()) {
					return;
				}

				if (System.currentTimeMillis() - startTime > 5000) {
					if (configX.getRatio() < 0.01 || configY.getRatio() < 0.01) {
						Log.d("za male wychylenia, jeszcze 5s ...");
						startTime = System.currentTimeMillis();
					} else {
						break;
					}
				}

				Rectangle pf = getFrame();
				if (pf != null) {
					processFrame = pf;
				}
			}

			final double usableX = configX.getUsableValue();
			final double usableY = configY.getUsableValue();

			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			final int width = (int) screenSize.getWidth();
			final int height = (int) screenSize.getHeight();

			cameraHandler.setResultListener(CameraHandler.SearchType.FACE, new CameraHandler.OnResultListener() {
				@Override
				public void onResult(Rectangle r, Point p) {
					if (p != null) {
						int movex = -ocX.getChange(p.x, usableX, width);
						if (movex != 0) {
							Point mousePos = MouseInfo.getPointerInfo().getLocation();
							robot.mouseMove(mousePos.x + movex, mousePos.y);
						}
					}
				}
			});

			cameraHandler.addResultListener(stForY, new CameraHandler.OnResultListener() {
				@Override
				public void onResult(Rectangle r, Point p) {
					if (p != null) {
						int movey = ocY.getChange(p.y, usableY, height);
						if (movey != 0) {
							Point mousePos = MouseInfo.getPointerInfo().getLocation();
							robot.mouseMove(mousePos.x, mousePos.y + movey);
						}
					}
				}
			});

			Log.d("go");
			while (isRunning()) {
				cameraHandler.getProcessFrame();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}