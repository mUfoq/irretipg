package handsfree;

/**
 *
 * @author ufoq
 */
public class ScalingConfigXY {

	private int cameraWidth, cameraHeight;
	private int usableWidth, usableHeight;
	private int minX, maxX;
	private int minY, maxY;

	public ScalingConfigXY(int cameraWidth, int cameraHeight) {
		this.cameraWidth = cameraWidth;
		this.cameraHeight = cameraHeight;
	}

	public void setCameraSize(int w, int h) {
		cameraWidth = w;
		cameraHeight = h;
	}

	public void setRangeX(int x1, int x2) {
		if (x2 > x1) {
			minX = x1;
			maxX = x2;
		} else {
			minX = x2;
			maxX = x1;
		}
		updateUsableSize();
	}

	public void setRangeY(int y1, int y2) {
		if (y2 > y1) {
			minY = y1;
			maxY = y2;
		} else {
			minY = y2;
			maxY = y1;
		}
		updateUsableSize();
	}

	public void recordX(int x) {
		if (x < minX) {
			minX = x;
		} else if (x > maxX) {
			maxX = x;
		}
		updateUsableSize();
	}

	public void recordY(int y) {
		if (y < minY) {
			minY = y;
		} else if (y > maxY) {
			maxY = y;
		}
		updateUsableSize();
	}

	public double getRatioWidth() {
		return usableWidth / (double) cameraWidth;
	}

	public double getRatioHeight() {
		return usableHeight / (double) cameraHeight;
	}

	private void updateUsableSize() {
		usableWidth = maxX - minX;
		usableHeight = maxY - minY;
	}
}
